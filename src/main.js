import {createApp} from 'vue';
import ElementPlus from 'element-plus';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import 'element-plus/dist/index.css';
import local from 'element-plus/es/locale/lang/zh-cn';
import {createPinia} from 'pinia';

import App from './App.vue';
// 全局样式
import '@/assets/styles/index.scss'
import "@/assets/iconfont/iconfont.css";
import Router from './router';
import i18n from '@/languages';

// 路由
const app = createApp(App);
app.use(Router);

//pina
app.use(createPinia());

app.use(i18n);

// ElementPlus组件
app.use(ElementPlus,{
  size: 'default',// 组件尺寸
  local: local // 默认语言
});

// Icon 图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
// 挂载
app.mount("#app");