import {defineStore} from 'pinia';
import {toRaw} from 'vue';
import AppConfig from '@/config/settings';

const useSettingsStore = defineStore('settings',{
  // 数据state
  state:() => ({
    title: AppConfig.title,
    theme: AppConfig.theme,
    sideTheme: AppConfig.sideTheme,
    showSettings: AppConfig.showSettings,
    topNav: AppConfig.topNav,
    tagsView: AppConfig.tagsView,
    fixedHeader: AppConfig.fixedHeader,
    sidebarLogo: AppConfig.sidebarLogo,
    dynamicTitle: AppConfig.dynamicTitle,
    errorLog: AppConfig.errorLog
  }),
  // 数据行为
  actions:{
    // 修改配置
    changeSetting(data){
      const {key,value} = data;
      if (this.hasProperty(key)){
        this[key] = value;
      }
    },
    // 设置标题
    settingTitle(title){
      this.title = title;
      useDynamicTitle();
    },
    // 检查属性是否存在
    hasProperty(propertyName) {
      return Object.prototype.hasOwnProperty.call(this, propertyName);
    }
  }
})

// 动态修改标题
function useDynamicTitle(){
  const {title,dynamicTitle} = useSettingsStore();
  if (dynamicTitle){
    document.title = title;
  }
}


export default useSettingsStore;