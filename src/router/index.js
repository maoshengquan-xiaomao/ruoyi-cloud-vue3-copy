import {createRouter,createWebHashHistory,createWebHistory} from 'vue-router';

const constantRoutes = [
  {
    path: '/',
    component: () => import('../layout')
  }
]

const Router = createRouter({
  history: createWebHistory(),
  routes: constantRoutes
})

export default Router;