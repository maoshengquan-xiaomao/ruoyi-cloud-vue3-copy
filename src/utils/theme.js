// 处理主题样式
function handleThemeStyle(theme) {
  document.documentElement.style.setProperty('--el-color-primary', theme)
  for (let i = 1; i <= 9; i++) {
    document.documentElement.style.setProperty(`--el-color-primary-light-${i}`, `${getLightColor(theme, i / 10)}`)
  }
  for (let i = 1; i <= 9; i++) {
    document.documentElement.style.setProperty(`--el-color-primary-dark-${i}`, `${getDarkColor(theme, i / 10)}`)
  }
}

// hex颜色转rgb颜色
function hexToRgb(str) {
  str = str.replace('#', '')
  let hex = str.match(/../g)
  for (let i = 0; i < 3; i++) {
    hex[i] = parseInt(hex[i], 16)
  }
  return hex
}

// rgb颜色转Hex颜色
function rgbToHex(r, g, b) {
  let hex = [r.toString(16), g.toString(16), b.toString(16)]
  for (let i = 0; i < 3; i++) {
    if (hex[i].length === 1) {
      hex[i] = `0${hex[i]}`
    }
  }
  return `#${hex.join('')}`
}

// 变浅颜色值
function getLightColor(color, level) {
  let rgb = hexToRgb(color)
  for (let i = 0; i < 3; i++) {
    rgb[i] = Math.floor((255 - rgb[i]) * level + rgb[i])
  }
  return rgbToHex(rgb[0], rgb[1], rgb[2])
}

// 变深颜色值
function getDarkColor(color, level) {
  let rgb = hexToRgb(color)
  for (let i = 0; i < 3; i++) {
    rgb[i] = Math.floor(rgb[i] * (1 - level))
  }
  return rgbToHex(rgb[0], rgb[1], rgb[2])
}

export {
  handleThemeStyle,
  hexToRgb,
  rgbToHex,
  getLightColor,
  getDarkColor
}
